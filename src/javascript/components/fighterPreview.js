import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

// todo: show fighter info (image, name, health, etc.)

  if(fighter){
    fighterElement.append( createFighterImage(fighter) )
    fighterElement.append( createFighterStats(fighter) )
  }
  
  return fighterElement;
}

/**
 * Returns img Element.
 *
 * @param {object} x The fighter data.
 * @return {domElement} x Img element
 */
export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterStats(fighter) {
  const { name, health, attack, defense } = fighter;
  const container = createElement({ tagName: 'div', className: 'fighter-stats-container' });
  const fighterName = createElement({ tagName: 'div', className: 'fighter-stats-name' });
  fighterName.innerText=`Name: ${name}`
  const fighterHealth = createElement({ tagName: 'div', className: 'fighter-stats-health' });
  fighterHealth.innerText=`❤️ Health: ${health}`
  const fighterAttack = createElement({ tagName: 'div', className: 'fighter-stats-attack' });
  fighterAttack.innerText=`🗡️ Attack: ${attack}`
  const fighterDefence = createElement({ tagName: 'div', className: 'fighter-stats-defence' });
  fighterDefence.innerText=`🛡️Defense: ${defense}`

  container.append(fighterName, fighterHealth, fighterAttack, fighterDefence);

  return container;
}
