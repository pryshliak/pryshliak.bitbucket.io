import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  //[TASK] call showModal function 
  const {winner, looser} = fighter
  const imageElement = createFighterImage(winner);
  const modalElement = {
    title: `${winner.name} won!`,
    bodyElement: imageElement,
    onClose : function(){
      location.reload()
    }
  };
  
  return showModal(modalElement); 

}
