import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

   /**
   * Returns x raised to the n-th power.
   *
   * @param {number} x The fighter id.
   * @return {object} x Fighter details
   */

  async getFighterDetails(id) {
    /* [TASK] 
      todo: implement this method
      endpoint - `details/fighter/${id}.json`;
    */
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
